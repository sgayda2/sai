#include "watcher.h"
#include <iostream>

Watcher::Watcher() {}

void Watcher::addPair(Signal* signal, unsigned int action)
{
    signal->signal_onFire().connect( sigc::bind<unsigned int>( sigc::mem_fun(*this, &Watcher::onFire), action ) );
    signals.push_back(signal);
}

void Watcher::update(double dt)
{
    std::map<unsigned int, unsigned int>::iterator it;
    for ( it=actions.begin() ; it != actions.end(); it++ )
    {
        if( (*it).second > dt )
            (*it).second -= dt;
        else
            (*it).second = 0;
    }

    //XXX make this more efficient
    for(unsigned int i = 0; i < signals.size(); i++)
        signals[i]->clearUpdated();

    for(unsigned int i = 0; i < signals.size(); i++)
        signals[i]->update(dt);
}

unsigned int Watcher::getAction() const
{   return current; }

sigc::signal<void> Watcher::signal_changed() const
{   return onChange;    }

void Watcher::onFire(unsigned int act)
{
    actions[act] += 5; //XXX this should be specified somewhere
    if( actions[act] >= actions[current] ) {
        current = act;
        onChange.emit();
    }
}

void Watcher::printMe()
{
    std::map<unsigned int, unsigned int>::iterator it;
    std::cout << "Watcher(" << current << ")" << std::endl;
    for ( it=actions.begin() ; it != actions.end(); it++ )
        std::cout << "\t" << (*it).first << " => " << (*it).second << std::endl;
}
