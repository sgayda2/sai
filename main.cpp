#include "signals.h"
#include "watcher.h"
#include <iostream>
#include <fstream>
#include <string>

#define _print(msg) std::cout << msg
#define _println(msg) std::cout << msg << std::endl

struct TSignal
{
    Signal* s;
    std::string nam;
    unsigned int act;
    TSignal() { s = new Signal();   }
    TSignal(const TSignal& other) { nam = other.nam; act = other.act; s = other.s; }
    void println() {  _println("(" << nam << "):" << act << "," << s->getValue()); }
    void print() {  _print("(" << nam << "):" << act << "," << s->getValue() << " "); }
};

bool setup(std::map<unsigned int, std::vector<unsigned int> >& info)
{
    std::ifstream myfile ("input.txt");
    if (myfile.is_open())
    {
        while ( 1 )
        {
            unsigned int frame;
            unsigned int sig;
            myfile >> frame;
            if ( myfile.eof() || myfile.fail() )
                break;
            myfile >> sig;
            if ( myfile.eof() || myfile.fail() )
                break;
            info[frame].push_back(sig);
        }
        myfile.close();
        return true;
    }
    else std::cout << "Unable to open file";

    return false;
}

void setupSigs(std::vector< TSignal >& sigs, Watcher& w)
{
#define make(x, a) TSignal x; x.nam = #x; x.act = a; sigs.push_back(x)
    make(one, 11);
    make(two, 22);
    make(three, 33);
    make(four, 44);

    TSignal t;
    t.nam = "one OR two";
    t.s = one.s->OR(two.s);
    t.act = 3;
    sigs.push_back(t);

    TSignal t2;
    t2.nam = "one AND three";
    t2.s = one.s->AND(three.s);
    t2.act = 13;
    sigs.push_back(t2);

    TSignal t3;
    t3.nam = "one AND three - AND four";
    t3.s = t2.s->AND(four.s);
    t3.act = 134;
    sigs.push_back(t3);

    w.addPair(one.s, one.act);
    w.addPair(two.s, two.act);
    w.addPair(three.s, three.act);
    w.addPair(t.s, t.act);
    w.addPair(t3.s, t3.act);
}

void wait()
{
    // Clear whatever's still in the buffer
    std::cin.clear();
    std::cout << "Press Enter to continue . . .\n";
    std::cin.get();
}

void printInfo(unsigned int frame, std::vector<unsigned int> list, Watcher &w)
{
    _println("frame: " << frame);
    _print("sigs updated: ( ");
    for(unsigned int i = 0; i < list.size(); i++)
        _print(list[i] << " ");
    _println(")");
    w.printMe();
}

void run(std::map<unsigned int, std::vector<unsigned int> >& info,
         std::vector< TSignal >& sigs, Watcher& w)
{
    unsigned int frame = 0;
    for(unsigned int i = 0; i < sigs.size(); i++)
        sigs[i].print();
    std::cout << std::endl << std::endl;

    while( true )
    {
        std::cout << "NEW FRAME" << std::endl;
        std::vector<unsigned int> list = info[frame];
        for(unsigned int i = 0; i < list.size(); i++)
            sigs[list[i]].s->setoff();

        printInfo(frame, list, w);
        for(unsigned int i = 0; i < sigs.size(); i++)
            sigs[i].print();
        std::cout << std::endl;

        wait();
        w.update(1);
        frame++;
    }
}

int main()
{
    std::map<unsigned int, std::vector<unsigned int> > info;
    if(!setup(info))
        return -1;

    std::vector< TSignal > sigs;
    Watcher w;

    setupSigs(sigs, w);

    run(info, sigs, w);

    return 0;
}
