#include "signals.h"

/************************************
            Signal class
************************************/

Signal::Signal(): value(0), updated(false) {}
Signal::Signal(const Signal& other) {}

void Signal::setoff()
{
    value += 5; //XXX this should not be static
    sensor.emit();
}

double Signal::getValue()
{   return value;   }

void Signal::clearUpdated()
{   updated = false;    }

void Signal::update(double dt)
{
    if( !updated ) {
        value -= dt;
        if(value < 0) value = 0;
        updated = true;
    }
}

sigc::signal<void> Signal::signal_onFire() const
{   return sensor;  }

Signal* Signal::AND(Signal* other)
{   return new AndSignal(this, other);  }

Signal* Signal::OR(Signal* other)
{   return new OrSignal(this, other);   }

/************************************
        AndSignal class
************************************/

AndSignal::AndSignal(Signal* a, Signal* b)
{
    left = a;
    right = b;
    a->signal_onFire().connect( sigc::mem_fun(*this, &AndSignal::onSignal));
    b->signal_onFire().connect( sigc::mem_fun(*this, &AndSignal::onSignal));
}

void AndSignal::onSignal()
{
    if( left->getValue() > 0 && right->getValue() > 0 )
        setoff();
}

void AndSignal::clearUpdated()
{
    Signal::clearUpdated(); //updated = false;
    right->clearUpdated();
    left->clearUpdated();
}

void AndSignal::update(double dt)
{
    Signal::update(dt); //value -= dt;
    right->update(dt);
    left->update(dt);
}

/************************************
         OrSignal class
************************************/

OrSignal::OrSignal(Signal* a, Signal* b)
{
    left = a;
    right = b;
    a->signal_onFire().connect( sigc::mem_fun(*this, &OrSignal::onSignal));
    b->signal_onFire().connect( sigc::mem_fun(*this, &OrSignal::onSignal));
}

void OrSignal::onSignal()
{   setoff();   }

void OrSignal::clearUpdated()
{
    Signal::clearUpdated(); //updated = false;
    right->clearUpdated();
    left->clearUpdated();
}

void OrSignal::update(double dt)
{
    Signal::update(dt); //value -= dt;
    right->update(dt);
    left->update(dt);
}
