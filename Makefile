CXX = g++
SRC = $(wildcard *.cpp)
OBJS = $(patsubst %.cpp, %.o, $(SRC))
CXXFLAGS += -c -g -Wall -O0
CXXFLAGS += `pkg-config --cflags sigc++-2.0`
LDFLAGS += `pkg-config --libs sigc++-2.0`
MAKEFLAGS += --no-print-directory

all: $(OBJS)
	$(CXX) $(LDFLAGS) $(OBJS) -o libsai

me: clean
	@$(MAKE)

%.o: %.cpp %.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<

main.o : main.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

clean:
	rm -f *.o libsai
