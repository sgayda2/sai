#ifndef __SAI_SIGNALS_H
#define __SAI_SIGNALS_H

#include <sigc++/sigc++.h>

class Signal
{
    public:
        // XXX copy and operator=
        // XXX add a paramater for lastfired
        Signal();
        void setoff();
        double getValue();
        Signal* AND(Signal* other);
        Signal* OR(Signal* other);
        virtual void update(double dt); //used to update time
        virtual void clearUpdated();
        sigc::signal<void> signal_onFire() const; //XXX rename this

    protected:
        Signal(const Signal& other);
        sigc::signal<void> sensor;
        double value;
        bool updated;
};

class AndSignal : public Signal
{
    public:
        // XXX copy and operator=
        AndSignal(Signal* left, Signal* right);
        virtual void update(double dt);
        virtual void clearUpdated();

    protected:
        void onSignal();
        Signal* left;
        Signal* right;
};

class OrSignal : public Signal
{
    public:
        // XXX copy and operator=
        OrSignal(Signal* left, Signal* right);
        virtual void update(double dt);
        virtual void clearUpdated();

    protected:
        void onSignal();
        Signal* left;
        Signal* right;
};

//XXX NotSignal??

#endif
