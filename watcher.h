#ifndef __SAI_WATCHER_H
#define __SAI_WATCHER_H

#include <vector>
#include <map>
#include <sigc++/sigc++.h>
#include "signals.h"

class Watcher
{
    public:
        // XXX copy and operator=
        Watcher();
        void addPair(Signal* signal, unsigned int actionId);
        void update(double dt);
        unsigned int getAction() const;
        sigc::signal<void> signal_changed() const;
        void printMe();

    private:
        unsigned int current;
        std::map<unsigned int, unsigned int> actions;
        std::vector< Signal* > signals;
        sigc::signal<void> onChange;
        void onFire(unsigned int act);
};

#endif
